﻿using Microsoft.EntityFrameworkCore;

namespace CineCancún.Models
{
    public class DbCont : DbContext 
    {
        public DbCont()
        {

        }
        public DbCont(DbContextOptions<DbCont> options) : base(options)
        {


        }
        public virtual DbSet<Cartelera> cartelera { get; set; }
        public virtual DbSet<Sala> sala { get; set; }

        protected override void onModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cartelera>(entity =>
            {
                entity.ToTable("Cartelera");
                entity.HasKey(e => e.idpelicula);
                entity.Property(e => e.idpelicula).HasColumnName("idpelicula");

                entity.Property(e => e.nombre)
                .HasColumnName("Nombre")
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

                entity.Property(e => e.categoria)
                .HasColumnName("Categoria")
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

                entity.Property(e => e.descripción)
                .HasColumnName("Descripción")
                .HasMaxLength(50)
                .IsUnicode(false);
            });

            modelBuilder.Entity<Sala>(entity =>
            {
                entity.ToTable("sala");
                entity.HasKey(c => c.idsala);
                entity.Property(c => c.idsala).HasColumnName("idsala");

                entity.Property(c => c.idpelicula).HasColumnName("idpelicula");

                entity.Property(c => c.nombre)
               .HasColumnName("Nombre")
               .IsRequired()
               .HasMaxLength(50)
               .IsUnicode(false);

                entity.HasOne(e => e.cartelera)
                .WithMany(c => c.sala)
                .HasForeignKey(c => c.idcartelera)
                .onDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Fk_sala_cartelera");

            });

            


        }

    }
}
